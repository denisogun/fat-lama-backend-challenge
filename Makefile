build:
	dep ensure
	go fmt src/*.go
	go test -coverprofile coverage.out ./src
	go tool cover -html coverage.out -o coverage.html
	env GOOS=linux go build -ldflags="-s -w" -o bin/search src/*.go

deploy: build
	serverless deploy function -f search
