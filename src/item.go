package main

import (
	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"

	"encoding/json"
	"errors"
	"os"
)

const (
	STAGE_ENV = "stage"
)

type Location struct {
	Latitude  float64 `json:"latitude"`
	Longitude float64 `json:"longitude"`
}

type Item struct {
	ItemName  string    `json:"itemName"`
	ImageURLs string    `json:"imageURLs"`
	Location  []float64 `json:"location"`
	URL       string    `json:"url"`
}

// Marshall the item to JSON. Note that we extract the location array [longitude, latitude]
// that is stored in elastic search, to a dictionary with latitude and longitude keys.
func (i *Item) MarshalJSON() ([]byte, error) {
	return json.Marshal(&struct {
		ItemName  string   `json:"itemName"`
		ImageURLs string   `json:"imageURLs"`
		Location  Location `json:"location"`
		URL       string   `json:"url"`
	}{
		ItemName:  i.ItemName,
		ImageURLs: i.ImageURLs,
		Location:  Location{Longitude: i.Location[0], Latitude: i.Location[1]},
		URL:       i.URL,
	})
}

// Take the ElasticSearch results and convert them to JSON. In dev this will return a pretty
// printed JSON but in prod it will be compacted.
func DecodeItems(q Query, results *elastic.SearchResult) (string, error) {
	if results == nil || results.TotalHits() == 0 {
		return "", errors.New("No search results were returned from ElasticSearch")
	}

	log.WithFields(log.Fields{
		"Query":         q.SearchTerm,
		"Lat":           q.Lat.Value,
		"Lng":           q.Lng.Value,
		"Hits":          results.TotalHits(),
		"Duration (ms)": results.TookInMillis}).Info("Fetched results")

	var items []*Item
	for _, hit := range results.Hits.Hits {
		item := new(Item)

		if err := json.Unmarshal(*hit.Source, item); err != nil {
			log.WithFields(log.Fields{
				"ItemName": item.ItemName,
				"URL":      item.URL,
				"Images":   item.ImageURLs,
				"Location": item.Location}).Warn("Error unmarshalling")
			continue
		}

		items = append(items, item)
	}

	var jsonString string
	var err error

	if os.Getenv(STAGE_ENV) == "prod" {
		jsonOutput, jsonError := json.Marshal(items)
		jsonString = string(jsonOutput)
		err = jsonError
	} else {
		jsonOutput, jsonError := json.MarshalIndent(items, "", "\t")
		jsonString = string(jsonOutput)
		err = jsonError
	}

	return jsonString, err
}
