package main

import (
	"github.com/stretchr/testify/assert"

	"errors"
	"testing"
)

func TestValidSearchNoLocationParameters(t *testing.T) {
	query := make(map[string]string)
	query["searchTerm"] = "hello"

	q, err := ParseQuery(query)

	if assert.NoError(t, err) {
		expectedQuery := Query{"hello", NullFloat{Value: 0, Valid: false}, NullFloat{Value: 0, Valid: false}}
		assert.Equal(t, expectedQuery, q)
	}
}

func TestNoSearchNoLocationParameters(t *testing.T) {
	query := make(map[string]string)

	_, err := ParseQuery(query)

	if assert.Error(t, err) {
		expectedError := errors.New("No search term or coordinate values")
		assert.Equal(t, expectedError, err)
	}
}

func TestInvalidSearchOneValidLocationParameters(t *testing.T) {
	query := make(map[string]string)
	query["lat"] = "51.509865"

	_, err := ParseQuery(query)

	if assert.Error(t, err) {
		expectedError := errors.New("No search term or coordinate values")
		assert.Equal(t, expectedError, err)
	}
}

func TestInvalidSearchValidLocationParameters(t *testing.T) {
	query := make(map[string]string)
	query["lat"] = "51.509865"
	query["lng"] = "-0.118092"

	q, err := ParseQuery(query)

	if assert.NoError(t, err) {
		expectedQuery := Query{SearchTerm: "", Lat: NullFloat{Value: 51.509865, Valid: true}, Lng: NullFloat{Value: -0.118092, Valid: true}}
		assert.Equal(t, expectedQuery, q)
	}
}

func TestInvalidSearchInvalidLongitudeParameters(t *testing.T) {
	query := make(map[string]string)
	query["lat"] = "51.509865"
	query["lng"] = "-180.1"

	_, err := ParseQuery(query)

	if assert.Error(t, err) {
		expectedError := errors.New("Invalid longitude Value")
		assert.Equal(t, expectedError, err)
	}
}

func TestInvalidSearchInvalidLatitudeParameters(t *testing.T) {
	query := make(map[string]string)
	query["lat"] = "90.1"
	query["lng"] = "-0.118092"

	_, err := ParseQuery(query)

	if assert.Error(t, err) {
		expectedError := errors.New("Invalid Latitude Value")
		assert.Equal(t, expectedError, err)
	}
}

func TestValidSearchInvalidLatitudeParameters(t *testing.T) {
	query := make(map[string]string)
	query["searchTerm"] = "hello"
	query["lat"] = "90.1"
	query["lng"] = "-0.118092"

	_, err := ParseQuery(query)

	if assert.Error(t, err) {
		expectedError := errors.New("Invalid Latitude Value")
		assert.Equal(t, expectedError, err)
	}
}

func TestValidSearchValidLocationParameters(t *testing.T) {
	query := make(map[string]string)
	query["searchTerm"] = "hello"
	query["lat"] = "51.509865"
	query["lng"] = "-0.118092"

	q, err := ParseQuery(query)

	if assert.NoError(t, err) {
		expectedQuery := Query{SearchTerm: "hello", Lat: NullFloat{Value: 51.509865, Valid: true}, Lng: NullFloat{Value: -0.118092, Valid: true}}
		assert.Equal(t, expectedQuery, q)
	}
}
