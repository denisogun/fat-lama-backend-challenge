package main

import (
	"github.com/olivere/elastic"
	"github.com/stretchr/testify/assert"

	"errors"
	"testing"
)

func TestDecodeNilHits(t *testing.T) {
	q := Query{"hello", NullFloat{Value: 0, Valid: true}, NullFloat{Value: 0, Valid: true}}

	_, err := DecodeItems(q, nil)

	if assert.Error(t, err) {
		expectedError := errors.New("No search results were returned from ElasticSearch")
		assert.Equal(t, expectedError, err)
	}
}

func TestDecodeNoHits(t *testing.T) {
	q := Query{"hello", NullFloat{Value: 0, Valid: true}, NullFloat{Value: 0, Valid: true}}
	results := &elastic.SearchResult{Hits: &elastic.SearchHits{TotalHits: 0}}
	_, err := DecodeItems(q, results)

	if assert.Error(t, err) {
		expectedError := errors.New("No search results were returned from ElasticSearch")
		assert.Equal(t, expectedError, err)
	}
}
