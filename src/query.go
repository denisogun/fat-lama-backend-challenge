package main

import (
	"github.com/olivere/elastic"
	log "github.com/sirupsen/logrus"

	"context"
	"errors"
	"os"
)

const (
	ELASTIC_ENDPOINT_ENV = "elasticEndpoint"
	indexName            = "items"
	numberResults        = 20
	searchPrefixLength   = 2
)

// Wrap float64 values with a boolean flag to indicate validity or not.
type NullFloat struct {
	Value float64
	Valid bool
}

type Query struct {
	SearchTerm string
	Lat        NullFloat
	Lng        NullFloat
}

// Compose the elastic search query based on the query we received from the user.
func (q Query) createElasticSearchQuery(client *elastic.Client) *elastic.SearchService {
	searchQuery := client.Search()
	searchQuery = searchQuery.Index(indexName)
	searchQuery = searchQuery.From(0)
	searchQuery = searchQuery.Size(numberResults)
	searchQuery = searchQuery.Pretty(true)

	if q.Lat.Valid == true && q.Lng.Valid == true {
		geoSortQuery := elastic.NewGeoDistanceSort("location")
		geoSortQuery = geoSortQuery.Point(q.Lat.Value, q.Lng.Value)
		geoSortQuery = geoSortQuery.Unit("km")
		geoSortQuery = geoSortQuery.GeoDistance("plane")
		geoSortQuery = geoSortQuery.Asc()

		searchQuery = searchQuery.SortBy(geoSortQuery)
	}

	if q.SearchTerm != "" {
		boolQuery := elastic.NewBoolQuery()
		// Exact match query
		exactMatchQuery := elastic.NewMatchQuery("itemName", q.SearchTerm)
		// Fuzzy match query for spelling mistakes
		fuzzyMatchQuery := elastic.NewMatchQuery("itemName", q.SearchTerm)
		fuzzyMatchQuery = fuzzyMatchQuery.Fuzziness("1")
		fuzzyMatchQuery = fuzzyMatchQuery.PrefixLength(searchPrefixLength)
		// Combine the two queries
		boolQuery = boolQuery.Should(exactMatchQuery, fuzzyMatchQuery)
		searchQuery = searchQuery.Query(boolQuery)
	}

	return searchQuery
}

// Execute the query on the elastic search instance and return the results as JSON
func (q Query) execute() (results string, err error) {
	log.WithFields(log.Fields{
		"ES Host": os.Getenv(ELASTIC_ENDPOINT_ENV)}).Info("Using elasticsearch host")

	client, err := elastic.NewClient(
		elastic.SetURL("https://"+os.Getenv(ELASTIC_ENDPOINT_ENV)),
		elastic.SetSniff(false),
	)

	if err != nil {
		return "", err
	}

	searchQuery := q.createElasticSearchQuery(client)
	res, err := searchQuery.Do(context.Background())

	if err != nil {
		return "", err
	}

	jsonResults, err := DecodeItems(q, res)

	return jsonResults, err
}

// Validate the user's input to make sure we have the right values and the values are valid
func (q Query) validate() error {
	// We don't have a search string and we have no location parameters, invalid
	if q.SearchTerm == "" && (q.Lat.Valid == false || q.Lng.Valid == false) {
		return errors.New("No search term or coordinate values")
	}

	// Check the Lat and lon values are Valid
	if q.Lat.Valid == true && q.Lat.Value < -90 || q.Lat.Value > 90 {
		return errors.New("Invalid Latitude Value")
	}

	if q.Lng.Valid == true && q.Lng.Value < -180 || q.Lng.Value > 180 {
		return errors.New("Invalid longitude Value")
	}

	// We have a search string and/or Valid location values
	return nil
}
