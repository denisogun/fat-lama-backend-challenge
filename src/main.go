package main

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	log "github.com/sirupsen/logrus"

	"strconv"
)

// ParseQuery takes in the search query, extracts and converts the coordinates to float64 values
// and returns a struct representing the query along with an error if required
func ParseQuery(search map[string]string) (q Query, err error) {
	var Lat, Lng NullFloat

	// Parse the Lat and lon strings. If they're invalid then set the Value to a Value out of range
	if LatVal, err := strconv.ParseFloat(search["lat"], 64); err != nil {
		log.Debug("Error parsing Latitude Value: " + err.Error())
		Lat = NullFloat{Value: 0, Valid: false}
	} else {
		Lat = NullFloat{Value: LatVal, Valid: true}
	}

	if LngVal, err := strconv.ParseFloat(search["lng"], 64); err != nil {
		log.Debug("Error parsing longitude Value: " + err.Error())
		Lng = NullFloat{Value: 0, Valid: false}
	} else {
		Lng = NullFloat{Value: LngVal, Valid: true}
	}

	q = Query{search["searchTerm"], Lat, Lng}
	e := q.validate()

	return q, e
}

// Handler process the APIGateway request and orchestrates the processing of the query
func Handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	q, err := ParseQuery(request.QueryStringParameters)

	// We couldn't parse the query
	if err != nil {
		log.WithFields(log.Fields{
			"query": q.SearchTerm,
			"Lat":   q.Lat.Value,
			"Lng":   q.Lng.Value}).Warn("Error handling query: " + err.Error())
		return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 400}, nil
	} else {
		// Execute the query on ElasticSearch
		result, err := q.execute()
		if err != nil {
			log.Warn("Error searching ES: " + err.Error())
			return events.APIGatewayProxyResponse{Body: err.Error(), StatusCode: 400}, nil
		}

		return events.APIGatewayProxyResponse{Body: result, StatusCode: 200}, nil
	}
}

func main() {
	lambda.Start(Handler)
}
