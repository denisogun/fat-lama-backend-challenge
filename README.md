# Summary

## Resources
- [Endpoint](https://1xc1jgr6xa.execute-api.eu-west-2.amazonaws.com/dev/search?searchTerm=camera&lat=51.948&lng=0.172943)
- [Git Repo](https://bitbucket.org/denisogun/fat-lama-backend-challenge/src/master/)

## Initial Thoughts

My initial thoughts are that this is a solved problem. Product search with geolocation are used in multiple websites and there are search systems that I am aware of such as ElasticSearch (ES) that I will need to do some more research into. While I could reinvent the wheel with an SQL-query based solution, it makes the scalability problem my problem. There are some good resources [on scalability](https://github.com/binhnguyennus/awesome-scalability) that specifically calls out the ElasticSearch stack

I will design my systems to allow for not having a search term but have a location to fulfil the user story of which items are near to me. Equally I want to be able to search for an item without adding a location.

## Implementation

I decided my implemtnation would use the Serverless framework which wraps around AWS Lambda and API gateway. It's a framework I hadn't used before so this acted as a good learning opportunity. It would also allow me to create the ES cluster within CloudFormation templates which is a good practice to deploy to a new region and for code reviewing infrastructure changes. I believe this solution will be scalable as the bottleneck will lie within the managed ElasticSearch service which has been built to be horizontally scalable. Making the search use Lambdas will also reduce dev ops work and potentially cost.

The first stage of my implementation was to convert the data from SQLite to JSON and I wrote a script to do this in /utilities. I then manually pushed this data to ElasticSearch to be indexed using the commands in the Appendix.

Now that the data was in ElasticSearch I installed various dependencies (dep, Go and serverless) and created my dev environment. With these installed and configured *make deploy* will format the code, run unit tests, calculate code coverage, build and deploy the code. I then started on writing the code which lives in /src. The majority of the code I needed to write was input validation, transformation and plumbing code in order to interact with the ElasticSearch cluster. This makes the lambda functions extremely lightweight, reducing memory and running time costs. It also followed that I focused my unit tests on the input validation as this is where I would have no control of inputs to functions.

With my solution created, it was time to test the API with various inputs and to verify the performance was acceptable. The results of 10 queries with varying search terms was tested and the lambda duration times in milliseconds were:

*180.73, 12.81, 14.73, 25.37, 45.9, 17.76, 18.86, 23.34, 21.86, 32.17*

This gives a mean execution time of 39.4ms (including the cold start outlier) and 21.3ms without the cold start issue.

## Next steps

I had to time box my implementation and so there are some gaps in my MVP that I'd correct before production. Namely:

- Further unit test and more importantly integration tests. Unit testing code that calls out to ElasticSearch is unwise and problems would be picked up by integration tests here
- The instance type and single instance could lead to outages and hitting limits quickly. This was done in the interest of cost
- The data needs to be ingested in another way such as a DynamoDB table where new items are stored. This is out of scope for the challenge
- Deploy the search system to different regions, add CI and stages
- Lock down the elastic search instance using IAM roles
- Make the API more robust: versioning, API keys and query metadata
- Solution for the cold start of Lambda functions

## Appendix

**Uploading data to ElasticSearch via CURL:**

curl -XDELETE https://search-fat-lama-search-d3ttoukoqyd4tbcdr4vlqqu44y.eu-west-2.es.amazonaws.com/items

curl -XPUT https://search-fat-lama-search-d3ttoukoqyd4tbcdr4vlqqu44y.eu-west-2.es.amazonaws.com/items/

curl -XPUT https://search-fat-lama-search-d3ttoukoqyd4tbcdr4vlqqu44y.eu-west-2.es.amazonaws.com/items/item/_mapping -H 'Content-Type: application/json' -d' {"item": {"properties": {"location": {"type": "geo_point"}}}}'  

curl -XPUT https://search-fat-lama-search-d3ttoukoqyd4tbcdr4vlqqu44y.eu-west-2.es.amazonaws.com/_bulk --data-binary @fatlama.json -H 'Content-Type: application/json'
