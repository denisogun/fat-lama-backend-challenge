#!/usr/bin/env python

import sqlite3
import jsonpickle

class Item(object):
    def __init__(self, name, lat, lng, url, img_urls):
        self.itemName = name
        self.location = [lng, lat]
        self.url = url
        img_urls = img_urls.replace('\'["', '').replace('"]\'', '')
        self.imageURLs = img_urls

def object_decoder(obj):
    if 'item_name' in obj and 'lat' in obj and 'lng' in obj and 'item_url' in obj and 'img_urls' in obj:
        return Item(obj['item_name'], obj['lat'], obj['lng'], obj['item_url'], obj['img_urls'])
    print("ERROR: Couldn't find all the items in " + obj)
    return obj

def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d

def index_string(idx):
    return '{{ "index" : {{ "_index" : "items", "_type" : "item", "_id" : "{}" }} }}'.format(idx)

connection = sqlite3.connect('./fatlama.sqlite3')
connection.row_factory = dict_factory
connection.text_factory = str
cursor = connection.cursor()

tableName = "items"
index = 1

print "Extracting the data from " + tableName
cursor.execute("SELECT * FROM " + tableName)

# Overwrite the file
with open("fatlama.json", 'w+') as file:
    for row in cursor:
        file.write(index_string(index) + '\n')
        index += 1
        item = object_decoder(row)
        if isinstance(item, Item):
            jsonObject = jsonpickle.encode(item, unpicklable=False)
            file.write(jsonObject + '\n')
