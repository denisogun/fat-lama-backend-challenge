#!/usr/bin/env python

import unittest
from sql_to_json import index_string

class TestIndexStringMethod(unittest.TestCase):
    def test_index_string_5(self):
        self.assertEqual(index_string(5), '{ "index" : { "_index" : "items", "_type" : "item", "_id" : "5" } }')

    def test_index_string_negative_1(self):
        self.assertEqual(index_string(-1), '{ "index" : { "_index" : "items", "_type" : "item", "_id" : "-1" } }')

    def test_index_string_none(self):
        self.assertEqual(index_string(None), '{ "index" : { "_index" : "items", "_type" : "item", "_id" : "None" } }')



if __name__ == '__main__':
    unittest.main()
